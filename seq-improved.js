// ==UserScript==
// @name        Seq Improved
// @namespace   NicroWare
// @match       https://seq.dev.*/
// @match       https://test.seq.*/
// @match       https://seq.*/
// @grant       none
// @version     1.4.2
// @author      Nicolas Fløysvik
// @description Adds support for marking in seq with using 1, 2, 3, 4, 5, 6, 7, 8, 9 or m
// @downloadUrl https://gitlab.com/nicro/violentmonkey-scripts/raw/master/seq-improved.js
// ==/UserScript==

let curItem = undefined;
let selectedItems = [];

let observer = undefined;
let mouseMoved = false;
let curKeyDown = undefined;
let lastAdded = undefined;
let firstDown = true;

document.body.addEventListener("mousemove", (e) => {
  curItem = e.target;
	while(curItem && !curItem.classList.contains("event")){
		curItem = curItem.parentElement;
  }
  if (curKeyDown) {
    mouseMoved = true;
    if (curItem && curItem !== lastAdded){
      addItem(curKeyDown);
      lastAdded = curItem;
    }
  }
});

function getColorForText(text) {
  var vals = selectedItems.filter(x => x.text == text);
  
  if (vals.length > 0) return vals[0].color;
  return null;
}

function updateMarking() {
  if (!observer) {
    const targetNode = document.getElementsByClassName("event-list")[0];
    const config = { childList: true };
    const callback = function (mutationsList, obs) {
      updateMarking();
    }
    observer = new MutationObserver(callback);
    observer.observe(targetNode, config);
  }
  
	[...document.getElementsByClassName("event")]
		.forEach(x => x.style.cssText = `background-color: ${getColorForText(x.textContent)} !important`);
}

const colors = { 
  "m": "rgb(255, 225, 171)",
  "1": "rgb(229, 255, 171)",
  "2": "rgb(181, 255, 171)",
  "3": "rgb(171, 235, 255)",
  "4": "rgb(170, 211, 255)",
  "5": "rgb(171, 176, 255)",
  "6": "rgb(217, 171, 255)",
  "7": "rgb(255, 171, 246)",
  "8": "rgb(255, 171, 171)",
  "9": "rgb(255, 203, 171)",
};

function getIndexFor(x, func) {
  for(let i = 0; i < x.length; i++) {
    if (func(x[i])) return i;
  }
  return -1;
}

function colorForKey(key) {
  
}

function addItem(key) {
  const curText = curItem.textContent;
  const curIndex = getIndexFor(selectedItems, x => x.text == curText);
  
  if (curIndex >= 0) {
    selectedItems.splice(curIndex, 1)
  } else {
    selectedItems.push({text: curItem.textContent, color: colors[key] });
  }
  updateMarking();
}

document.body.addEventListener("keydown", (e) => {
  if (firstDown) {
    mouseMoved = false;
    firstDown = false;
  }
	switch(e.key) {
		case "m":
    case "1":
    case "2":
    case "3":
    case "4":
    case "5":
    case "6":
    case "7":
    case "8":
    case "9":
      curKeyDown = e.key;
			break;
	}
});

document.body.addEventListener("keyup", (e) => {
  if (curItem) {
    if (!mouseMoved) {
      switch(e.key) {
        case "m":
        case "1":
        case "2":
        case "3":
        case "4":
        case "5":
        case "6":
        case "7":
        case "8":
        case "9":
          if (!mouseMoved) {
            addItem(e.key);
          }
          break;
        case "u":
          updateMarking()
          break;
      }
    }
  }
  mouseMoved = false;
  curKeyDown = undefined;
  lastAdded = undefined;
  firstDown = true;
});